<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/produtos', 'ControladorProdutos@index');
Route::get('/produto/novo', 'ControladorProdutos@create');
Route::post('/produtos', 'ControladorProdutos@store');
Route::get('/produto/excluir/{id}', 'ControladorProdutos@destroy');
Route::get('/produto/editar/{id}', 'ControladorProdutos@edit');
Route::post('/produto/editar/{id}', 'ControladorProdutos@update');


Route::get('/departamentos', 'ControladorDepartamentos@index');
Route::get('/departamentos/novo', 'ControladorDepartamentos@create');
Route::post('/departamentos', 'ControladorDepartamentos@store');
Route::get('/departamento/excluir/{id}', 'ControladorDepartamentos@destroy');
Route::get('/departamento/editar/{id}', 'ControladorDepartamentos@edit');
Route::post('/departamento/editar/{id}', 'ControladorDepartamentos@update');
