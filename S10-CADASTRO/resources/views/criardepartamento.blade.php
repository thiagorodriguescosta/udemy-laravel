@extends('layout.app', ['pageCurrent' => 'departamentos'])

@section('title-page', 'Novo Departamento')

@section('body')
    <div class="card text-left">
        <div class="card-body">
            <form action="/departamentos" method="post">
                @csrf
                <div class="form-group">
                    <label for="name">Nome</label>
                    <input type="text" class="form-control" name="name" id="name" aria-describedby="helpId" placeholder="Nome do Departamento">
                </div>
                <button type="submit" class="btn btn-primary">Salvar</button>
            </form>
        </div>
    </div>
@endsection