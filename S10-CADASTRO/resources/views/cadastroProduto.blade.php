@extends('layout.app', ['pageCurrent' => 'produto'])

@section('title-page', 'Cadastro de Produto')

@section('body')
    <div class="card">
        <div class="card-body">
            <form action="/produtos" method="post">
                @csrf
                <div class="form-group">
                    <label for="descricao">Descrição</label>
                    <input type="text" name="descricao" id="descricao" class="form-control" placeholder="Descrição" aria-describedby="helpId">
                </div>
                <div class="form-group">
                    <label for="idCategoria">Categoria</label>
                    <select class="form-control" name="idCategoria" id="idCategoria">
                        <option>Selecionar...</option>
                        @foreach ($departamentos as $departamento)
                        <option value="{{$departamento->id}}">{{$departamento->nome}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="preco">Preço</label>
                    <input type="number" name="preco" id="preco" class="form-control" placeholder="Preço" aria-describedby="helpId">
                </div>
                <button type="submit" class="btn btn-primary">Salvar</button>
            </form>
        </div>
    </div>
@endsection