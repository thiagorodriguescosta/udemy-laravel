@extends('layout.app', ['pageCurrent' => 'produto'])

@section('title-page', 'Editar Produto')

@section('body')
    <div class="card">
        <div class="card-body">
            <form action="/produto/editar/{{$produto->id}}" method="post">
                @csrf
                <div class="form-group">
                    <label for="descricao">Descrição</label>
                    <input type="text" name="descricao" id="descricao" value="{{$produto->descricao}}" class="form-control" placeholder="Descrição" aria-describedby="helpId">
                </div>
                <div class="form-group">
                    <label for="idCategoria">Categoria</label>
                    <select class="form-control" name="idCategoria" id="idCategoria">
                        <option>Selecionar...</option>
                        @foreach ($departamentos as $departamento)
                        <option value="{{$departamento->id}}" @if ($produto->departamento_id == $departamento->id)
                            selected="selected"
                        @endif>{{$departamento->nome}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label for="preco">Preço</label>
                    <input type="number" name="preco" id="preco" value="{{$produto->preco}}" class="form-control" placeholder="Preço" aria-describedby="helpId">
                </div>
                <button type="submit" class="btn btn-primary">Salvar</button>
            </form>
        </div>
    </div>
@endsection