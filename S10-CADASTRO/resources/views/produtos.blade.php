@extends('layout.app', ["pageCurrent" => "produtos"])

@section('title-page', 'Página de Produtos')

@section('body')
    <div class="card card-border">
      <div class="card-body">
        <h4 class="card-title">Listagem de Produtos</h4>
        <table class="table">
            <thead>
                <tr>
                    <th>Código</th>
                    <th>Descrição</th>
                    <th>Categoria</th>
                    <th>Preço</th>
                    <th>Ações</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($produtos as $produto)
                <tr>
                    <td scope="row">{{$produto->id}}</td>
                    <td>{{$produto->descricao}}</td> 
                    <td>{{$produto->departamento_id}}</td>
                    <td>{{$produto->preco}}</td>    
                    <td>
                        <a class="btn btn-primary" href="/produto/editar/{{$produto->id}}" role="button">Editar</a>    
                        <a class="btn btn-danger" href="/produto/excluir/{{$produto->id}}" role="button">Excluir</a>    
                    </td>    
                </tr>
                @endforeach
            </tbody>
        </table>
      </div>
      <div class="card-footer">
          <a class="btn btn-primary" href="/produto/novo" role="button">Criar Produto</a>
      </div>
    </div>
@endsection


