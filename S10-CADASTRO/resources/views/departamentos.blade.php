@extends('layout.app', ["pageCurrent" => 'departamentos'])

@section('title-page', 'Página de Categorias')

@section('body')
    <div class="card border">
        <div class="card-body">
            <h4 class="card-title">Listar de Departamentos</h4>
            @if (count($departamentos) > 0)
            <table class="table">
                <thead>
                    <tr>
                        <th>CÓDIGO</th>
                        <th>NOME</th>
                        <th>AÇÕES</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($departamentos as $departamento)    
                    <tr>
                        <td>{{$departamento->id}}</td>
                        <td>{{$departamento->nome}}</td>
                        <td>
                            <a class="btn btn-primary" href="/departamento/editar/{{$departamento->id}}">Editar</a>
                            <a class="btn btn-danger" href="/departamento/excluir/{{$departamento->id}}">Excluir</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            @endif
        </div>
        <div class="card-footer">
            <a name="criarDepartamento" id="criarDepartamento" class="btn btn-primary" href="/departamentos/novo" role="button">Criar Departamento</a>
        </div>
    </div>
@endsection


