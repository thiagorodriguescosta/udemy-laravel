@extends('layout.app', ['pageCurrent' => 'index'])

@section('title', 'Cadastro de Produto')

@section('body')
    <div class="jumbotron bg-light border-secondary">
        <div class="card-deck">
            <div class="card">
                <img class="card-img-top" src="holder.js/100x180/" alt="">
                <div class="card-body">
                    <h4 class="card-title">Cadastro de Produto</h4>
                    <p class="card-text">
                        Cadastre seus produtos por aqui
                    </p>
                    <a href="/produto" class="btn btn-primary">Cadastre seus Produtos</a>
                </div>
            </div>
            <div class="card">
                <img class="card-img-top" src="holder.js/100x180/" alt="">
                <div class="card-body">
                    <h4 class="card-title">Cadastro de Departamento</h4>
                    <p class="card-text">
                        Cadastre seus departamentos por aqui
                    </p>
                    <a href="/departamento" class="btn btn-primary">Cadastre seus Departamentos</a>
                </div>
            </div>
        </div>
    </div>
@endsection