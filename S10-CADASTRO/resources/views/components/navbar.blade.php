<nav class="navbar navbar-expand-sm navbar-dark bg-dark">
    
    <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar"
        aria-expanded="false" aria-label="Toggle navigation"></button>
    <div class="collapse navbar-collapse" id="navbar">
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
            <li class="nav-item active">
                <a class="nav-link" href="/">Home @if ($pageCurrent == "index")<span class="sr-only">(current)</span>@endif</a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="/produtos">Produtos @if ($pageCurrent == "produtos")<span class="sr-only">(current)</span>@endif</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/departamentos">Departamentos @if ($pageCurrent == "departamentos")<span class="sr-only">(current)</span>@endif</a>
            </li>
        </ul>
    </div>
</nav>