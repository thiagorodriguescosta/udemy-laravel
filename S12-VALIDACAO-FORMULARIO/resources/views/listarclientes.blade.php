<!DOCTYPE html>
<html lang="pt-br">
<head>
    <title>Listar Clientes</title>
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body class="p-2">
    <main>
        <div class="row">
            <div class="container col-md-8 offset-md-2">
                <div class="card">
                    <div class="card-header">
                        Listar Clientes
                    </div>
                    <div class="card-body">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>CÓDIGO</th>
                                    <th>NOME</th>
                                    <th>IDADE</th>
                                    <th>EMAIL</th>
                                    <th>ENDEREÇO</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($clientes as $cliente)
                                <tr>
                                    <td scope="row">{{ $cliente->id }}</td>
                                    <td>{{ $cliente->nome }}</td>
                                    <td>{{ $cliente->idade }}</td>
                                    <td>{{ $cliente->email }}</td>
                                    <td>{{ $cliente->endereco }}</td>
                                </tr>    
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <script src="{{ asset('js/app.js') }}" type="text/javascript"></script>
</body>
</html>