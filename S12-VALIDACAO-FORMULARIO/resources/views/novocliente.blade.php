    <!DOCTYPE html>
    <html lang="pt-br">
    <head>
        <title>Cadastro de cliente</title>
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <meta name="csrf-token" content="{{ csrf_token() }}">
    </head>
    <body class="p-2">
        <main>
            <div class="row">
                <div class="container col-md-8 offset-md-2">
                    <div class="card">
                        <div class="card-header">
                            Cadastro de Cliente
                        </div>
                        <div class="card-body">
                            <form action="/cliente" method="post">
                                @csrf
                                <div class="form-group">
                                    <label for="nome">Nome do Cliente</label>
                                    <input type="text" name="nome" id="nome" class="form-control {{ $errors->has('nome') ? 'is-invalid' : '' }}" placeholder="Nome do Cliente" aria-describedby="helpId">
                                    <div class="invalid-feedback">
                                        @if ($errors->has('nome'))
                                            {{ $errors->first('nome') }}
                                        @endif
                                    </div>

                                </div>
                                <div class="form-group">
                                    <label for="idade">Idade do Cliente</label>
                                    <input type="number" name="idade" id="idade" class="form-control {{ $errors->has('idade') ? 'is-invalid' : '' }}" placeholder="Idade do Cliente" aria-describedby="helpId">
                                    <div class="invalid-feedback">
                                        @if ($errors->has('idade'))
                                            {{ $errors->first('idade') }}
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="endereco">Endereço do Cliente</label>
                                    <input type="text" name="endereco" id="endereco" class="form-control {{ $errors->has('endereco') ? 'is-invalid' : '' }}" placeholder="Endereço do Cliente" aria-describedby="helpId">
                                    <div class="invalid-feedback">
                                        @if ($errors->has('endereco'))
                                            {{ $errors->first('endereco') }}
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="email">Email do Cliente</label>
                                    <input type="text" name="email" id="email" class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" placeholder="Email do Cliente" aria-describedby="helpId">
                                    <div class="invalid-feedback">
                                        @if ($errors->has('email'))
                                            {{ $errors->first('email') }}
                                        @endif
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary btn-sm">Salvar</button>
                                <button type="cancel" class="btn btn-primary btn-sm">Cancelar</button>
                            </form>
                        </div>
                        {{-- <div class="card-footer">
                            @if (isset($errors))
                                @foreach ($errors->all() as $error)
                            <div class="alert alert-danger" role="alert">
                                <strong>{{ $error }}</strong>
                            </div>
                                @endforeach
                            @endif
                        </div> --}}
                    </div>
                </div>
            </div>
        </main>
        <script src="{{ asset('js/app.js') }}" type="text/javascript"></script>
    </body>
    </html>