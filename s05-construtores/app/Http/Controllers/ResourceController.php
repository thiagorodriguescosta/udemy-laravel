<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ResourceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return 'Visualizar listagem (GET)';
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return 'Formulário de cadastro (GET)';
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $nome  = $request->input("name");
        $idade = $request->input("age");
        
        return "Alterar cadastro nome: $nome idade: $idade (POST)";
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cadastro = array("João", "Marta", "Pedro", "Paulo");
        if ($id <= 0 && $id >= count($cadastro)) {
            return "Visualizar cadastro" . $cadastro[$id] . " (GET)";
        }
        return "cadastro não encontrado (GET)";
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return "Formulário de edição do cadastro (GET) $id";
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $nome  = $request->input("name");
        $idade = $request->input("age");

        return "Atualizando nome: $nome, idade: $idade cadastro (PUT|PATCH) $id";
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return "Deletando cadastro (DELETE) $id";
    }
}
