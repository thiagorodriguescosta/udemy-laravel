<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/** 
 * GET PARA CONTROLLER 
*/
Route::get('/listar-produto', 'MeuControlador@getNome');

Route::get('/visualizar-produto/{id}', 'MeuControlador@getProduto');

/**
 * RESOURCE - TODOS OS MÉTODOS HTTP
 */
Route::resource('/resource', 'ResourceController');

