<?php

use Illuminate\Database\Seeder;

class CategoriaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categorias')->insert([
            'nome' => 'TV'
        ]);
        DB::table('categorias')->insert([
            'nome' => 'Rádio'
        ]);
        DB::table('categorias')->insert([
            'nome' => 'Computador'
        ]);
        DB::table('categorias')->insert([
            'nome' => 'Eletrônicos'
        ]);
    }
}
