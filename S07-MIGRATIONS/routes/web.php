<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/categorias', function() {
    $categorias = DB::table('categorias')->get();

    echo "<h3>Todas categorias com todas linhas e colunas</h3>";
    foreach($categorias as $categoria) {
        echo "id: " . $categoria->id . " ";
        echo "nome: " . $categoria->nome . "<br>";
    }
    echo "<hr>";

    echo "<h3>Todas categorias com todas linhas e coluna nome</h3>";
    $nomesCategoria = DB::table('categorias')->pluck('nome');
    foreach($nomesCategoria as $nomeCategoria) {
        echo "nome: " . $nomeCategoria . "<br>";
    }
    echo "<hr>";

    echo "<h3>Primeira categoria com todas as linhas e todas colunas</h3>";
    $cat = DB::table('categorias')->where('id', 1)->get();
    echo "id: " . $cat[0]->id . " ";
    echo "nome: " . $cat[0]->nome . "<br>";
    echo "<hr>";

    echo "<h3>Primeira categoria com todas as linhas e todas colunas</h3>";
    $cat = DB::table('categorias')->where('id', 1)->first();
    echo "id: " . $cat->id . " ";
    echo "nome: " . $cat->nome . "<br>";
    echo "<hr>";

    echo "<h3>Utilizando (LIKE)</h3>";
    $categorias = DB::table('categorias')->where('nome', 'like', '%a%')->get();
    foreach($categorias as $categoria) {
        echo "id: " . $categoria->id . " ";
        echo "nome: " . $categoria->nome . "<br>";
    }
    echo "<hr>";
    
    echo "<h3>Utilizando (OR)</h3>";
    $categorias = DB::table('categorias')->where('id', 1)->orWhere('id', 2)->get();
    foreach($categorias as $categoria) {
        echo "id: " . $categoria->id . " ";
        echo "nome: " . $categoria->nome . "<br>";
    }
    echo "<hr>";

    echo "<h3>Utilizando (AND)</h3>";
    $categorias = DB::table('categorias')->where([
        ['id', 1],
        ['nome', 'TV']
    ])->get();
    foreach($categorias as $categoria) {
        echo "id: " . $categoria->id . " ";
        echo "nome: " . $categoria->nome . "<br>";
    }
    echo "<hr>";
    
    echo "<h3>Utilizando (BETWEEN)</h3>";
    $categorias = DB::table('categorias')->whereBetween('id', [1,3])->get();
    foreach($categorias as $categoria) {
        echo "id: " . $categoria->id . " ";
        echo "nome: " . $categoria->nome . "<br>";
    }
    echo "<hr>";
    
    echo "<h3>Utilizando (NOT BETWEEN)</h3>";
    $categorias = DB::table('categorias')->whereNotBetween('id', [1,3])->get();
    foreach($categorias as $categoria) {
        echo "id: " . $categoria->id . " ";
        echo "nome: " . $categoria->nome . "<br>";
    }
    echo "<hr>";
    
    echo "<h3>Utilizando (IN)</h3>";
    $categorias = DB::table('categorias')->whereIn('id', [1,3,4])->get();
    foreach($categorias as $categoria) {
        echo "id: " . $categoria->id . " ";
        echo "nome: " . $categoria->nome . "<br>";
    }
    echo "<hr>";
    
    echo "<h3>Utilizando (NOT IN)</h3>";
    $categorias = DB::table('categorias')->whereNotIn('id', [1,3,4])->get();
    foreach($categorias as $categoria) {
        echo "id: " . $categoria->id . " ";
        echo "nome: " . $categoria->nome . "<br>";
    }
    echo "<hr>";

    echo "<h3>Utilizando (ORDER BY ASC)</h3>";
    $categorias = DB::table('categorias')->orderBy('nome')->get();
    foreach($categorias as $categoria) {
        echo "id: " . $categoria->id . " ";
        echo "nome: " . $categoria->nome . "<br>";
    }
    echo "<hr>";

    echo "<h3>Utilizando (ORDER BY DESC)</h3>";
    $categorias = DB::table('categorias')->orderBy('nome', 'desc')->get();
    foreach($categorias as $categoria) {
        echo "id: " . $categoria->id . " ";
        echo "nome: " . $categoria->nome . "<br>";
    }
    echo "<hr>";
});


Route::get('/insertcategorias', function() {
    DB::table('categorias')->insert([
        ['nome' => 'Violão'],
        ['nome' => 'Aparelho de Som'],
        ['nome' => 'Casa Mesa e Banho']
    ]);

    $idInserido = DB::table('categorias')->insertGetId(['nome' => 'carros']);

    echo "Novo id inserido: " . $idInserido;
});

Route::get('/updatecategorias', function() {

    echo "<h2> Registro antes da atualização </h2>";
    $categoria = DB::table('categorias')->first();
    echo "id: " . $categoria->id . " ";
    echo "nome: " . $categoria->nome . "<br>";
    echo "<hr>";

    DB::table('categorias')->where('id', 1)->update(['nome' => 'Televisão']);

    echo "<h2> Registro depois da atualização </h2>";
    $categoria = DB::table('categorias')->first();
    echo "id: " . $categoria->id . " ";
    echo "nome: " . $categoria->nome . "<br>";
    echo "<hr>";
});

Route::get('/deletecategorias', function() {
    echo "<h2>ANTES da exclusão</h2>";
    $categorias = DB::table('categorias')->get();
    foreach($categorias as $categoria) {
        echo "id: " . $categoria->id . " ";
        echo "nome: " . $categoria->nome . "<br>";
    }

    // DB::table('categorias')->where('id', 1)->delete();
    DB::table('categorias')->whereNotIn('id', [2, 3, 4, 5, 6])->delete();

    echo "<h2>DEPOIS da exclusão</h2>";
    $categorias = DB::table('categorias')->get();
    foreach($categorias as $categoria) {
        echo "id: " . $categoria->id . " ";
        echo "nome: " . $categoria->nome . "<br>";
    }
});
