<?php

use Illuminate\Database\Seeder;

class CategoriasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categorias')->insert([
            'nome' => 'Instrumentos Musicais'
        ]);
        DB::table('categorias')->insert([
            'nome' => 'Jogos'
        ]);
        DB::table('categorias')->insert([
            'nome' => 'Eletrônicos'
        ]);
        DB::table('categorias')->insert([
            'nome' => 'Jardinagem'
        ]);
        DB::table('categorias')->insert([
            'nome' => 'Cozinha'
        ]);
    }
}
