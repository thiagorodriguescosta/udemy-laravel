<?php

use Illuminate\Database\Seeder;

class ProdutosSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('produtos')->insert([
            'nome' => 'Devil may cry 4',
            'preco' => 99.99,
            'estoque' => 5,
            'categoria_id' => 2
        ]);
        DB::table('produtos')->insert([
            'nome' => 'Jogo de Panelas',
            'preco' => 300.99,
            'estoque' => 25,
            'categoria_id' => 5
        ]);
        DB::table('produtos')->insert([
            'nome' => 'Guitarra Elétrica',
            'preco' => 5000.98,
            'estoque' => 2,
            'categoria_id' => 1
        ]);
        DB::table('produtos')->insert([
            'nome' => 'TV Samsung',
            'preco' => 5040.99,
            'estoque' => 3,
            'categoria_id' => 3
        ]);
        DB::table('produtos')->insert([
            'nome' => 'Regador',
            'preco' => 9.99,
            'estoque' => 50,
            'categoria_id' => 4
        ]);
        DB::table('produtos')->insert([
            'nome' => 'Monitor 40 pol',
            'preco' => 630.00,
            'estoque' => 5,
            'categoria_id' => 3
        ]);
    }
}
