<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/clientes', function () {
    $results = App\Cliente::all();

    foreach ($results as $result) {
        echo "<p>Nome: " . $result->nome . "</p>";
        echo "<p>Telefone: " . $result->telefone . "</p>";

        $e = App\Endereco::where('cliente_id', $result->id)->first();
        echo "<p>Cep: " . $e->cep . "</p>";
        echo "<p>Logradouro: " . $e->logradouro . "</p>";
        echo "<p>Cidade: " . $e->cidade . "</p>";
        echo "<p>Bairro: " . $e->bairro . "</p>";
        echo "<p>Estado: " . $e->estado . "</p>";
        echo "<p>Número: " . $e->numero . "</p>";
        echo "<hr>";
    }
});

/**
 * utilizando o hasOne
 */
Route::get('/clienteshasOne', function () {
    $results = App\Cliente::all();

    foreach ($results as $result) {
        echo "<p>Nome: " . $result->nome . "</p>";
        echo "<p>Telefone: " . $result->telefone . "</p>";
        echo "<p>Cep: " . $result->endereco->cep . "</p>";
        echo "<p>Logradouro: " . $result->endereco->logradouro . "</p>";
        echo "<p>Cidade: " . $result->endereco->cidade . "</p>";
        echo "<p>Bairro: " . $result->endereco->bairro . "</p>";
        echo "<p>Estado: " . $result->endereco->estado . "</p>";
        echo "<p>Número: " . $result->endereco->numero . "</p>";
        echo "<hr>";
    }
});

Route::get('/enderecos', function () {
    $results = App\Endereco::all();

    foreach ($results as $result) {
        echo "<p>Cep: " . $result->cep . "</p>";
        echo "<p>Logradouro: " . $result->logradouro . "</p>";
        echo "<p>Cidade: " . $result->cidade . "</p>";
        echo "<p>Bairro: " . $result->bairro . "</p>";
        echo "<p>Estado: " . $result->estado . "</p>";
        echo "<p>Número: " . $result->numero . "</p>";
        echo "<hr>";
    }
}); 

/**
 * Utilizando o belongsTo
 */

Route::get('/enderecosbelongsTo', function () {
    $results = App\Endereco::all();

    foreach ($results as $result) {
        echo "<p>Nome: " . $result->cliente->nome . "</p>";
        echo "<p>Telefone: " . $result->cliente->telefone . "</p>";

        echo "<p>Cep: " . $result->cep . "</p>";
        echo "<p>Logradouro: " . $result->logradouro . "</p>";
        echo "<p>Cidade: " . $result->cidade . "</p>";
        echo "<p>Bairro: " . $result->bairro . "</p>";
        echo "<p>Estado: " . $result->estado . "</p>";
        echo "<p>Número: " . $result->numero . "</p>";
        echo "<hr>";
    }
}); 

Route::get('/inserir', function() {
    $cliente = new App\Cliente();

    $cliente->nome = 'Rodrigo Longo';
    $cliente->telefone = '4523046506';

    $cliente->save();


    $endereco = new App\Endereco();

    $endereco->cep = 7054660;
    $endereco->logradouro = "Rua Santos Dumont";
    $endereco->numero = "502";
    $endereco->bairro = "Centro";
    $endereco->cidade = "Londrina";
    $endereco->estado = "São Paulo";
    $endereco->cliente_id = $cliente->id;

    $endereco->save();



    $cliente = new App\Cliente();

    $cliente->nome = 'Juliano Longo';
    $cliente->telefone = '4523046506';

    $cliente->save();


    $endereco = new App\Endereco();

    $endereco->cep = 7054660;
    $endereco->logradouro = "Rua Santos Dumont";
    $endereco->numero = "502";
    $endereco->bairro = "Centro";
    $endereco->cidade = "Londrina";
    $endereco->estado = "São Paulo";

    $cliente->endereco()->save($endereco);
});

Route::get('/clientesEager', function() {
    return App\Cliente::with(['endereco'])->get();
});

Route::get('/enderecosEager', function() {
    return App\Endereco::with(['cliente'])->get();
});

/**
 * Utilizando o belongsTo
 */

Route::get('/produtosbelongsTo', function () {
    $results = App\Produtos::all();

    foreach ($results as $result) {
        echo "<p>Nome: " . $result->nome . "</p>";
        echo "<p>Preço: " . $result->preco . "</p>";
        echo "<p>Estoque: " . $result->estoque . "</p>";
        echo "<p>Categoria: " . $result->categorias->nome . "</p>";
        echo "<hr>";
    }
}); 

/**
 * Utilizando o hasMany
 */
Route::get('/categoriashasMany', function () {
    $results = App\Categorias::all();

    foreach ($results as $result) {
        echo "<p>Nome: " . $result->nome . "</p>";

        $produtos = $result->produtos;

        foreach ($produtos as $produto) {

            echo "<p>Nome: " . $produto->nome . "</p>";
            echo "<p>Preço: " . $produto->preco . "</p>";
            echo "<p>Estoque: " . $produto->estoque . "</p>";
        }
        echo "<hr>";
    }
}); 

Route::get('/categoriashasManyEager', function () {
    return App\Categorias::with('produtos')->get();
});

Route::get('/inserirprodutocomcategoriaid/{idCategoria}', function($idCategoria) {
    $categoria = App\Categorias::find($idCategoria);
    $produto = new App\Produtos();

    $produto->nome = 'Teste';
    $produto->preco = 90.60;
    $produto->estoque = 20;
    $produto->categorias()->associate($categoria);
    $produto->save();

    return $produto;
});

Route::get('/removendocategoriadoproduto/{idProduto}', function($idProduto) {
    $produto = App\Produtos::find($idProduto);
    if (isset($produto)) {
        $produto->categorias()->dissociate();
        $produto->save();
    }

    return $produto;
});

Route::get('/inserirprodutopelacategoria/{idCategoria}', function($idCategoria) {
    $categoria = App\Categorias::with('produtos')->find($idCategoria);
    $produto = new App\Produtos();

    $produto->nome = 'Teste produto';
    $produto->preco = 90.60;
    $produto->estoque = 20;

    $categoria->produtos()->save($produto);
    $categoria->load('produtos');

    return $categoria;
});

/**
  * Utilizando o belongsToMany
 */

use App\Desenvolvedor;

Route::get('/devenvolvedoresbelongsToMany', function() {
    $devenvolvedores = Desenvolvedor::with('projetos')->get();

    foreach($devenvolvedores as $desenvolvedor) {
        echo "<p>Nome: " . $desenvolvedor->nome . "</p>";
        echo "<p>Cargo: " . $desenvolvedor->cargo . "</p>";

        echo "<ul>";
        foreach($desenvolvedor->projetos as $projeto) {
            echo "<li>Nome:" . $projeto->nome . "</li>";
            echo "<li>Estimativa:" . $projeto->estimativa_horas . "</li>";
            echo "<li>Horas Semanais:" . $projeto->pivot->horas_semanais . "</li>";
            echo "<li>Data Criação:" . $projeto->pivot->created_at . "</li>";
        }
        echo "</ul>";

        echo "<hr>";
    }
});

use App\Projeto;

Route::get('/projetosbelongsToMany', function() {
    $projetos = Projeto::with('desenvolvedores')->get();

    foreach($projetos as $projeto) {
        echo "<li>Nome:" . $projeto->nome . "</li>";
        echo "<li>Estimativa:" . $projeto->estimativa_horas . "</li>";
        
        echo "<ul>";
        foreach($projeto->desenvolvedores as $desenvolvedor) {
            echo "<p>Nome: " . $desenvolvedor->nome . "</p>";
            echo "<p>Cargo: " . $desenvolvedor->cargo . "</p>";
            echo "<li>Horas Semanais:" . $desenvolvedor->pivot->horas_semanais . "</li>";
            echo "<li>Data Criação:" . $desenvolvedor->pivot->created_at . "</li>";
            echo "<hr>";
        }
        echo "</ul>";

        echo "<hr>";
    }
});

Route::get('/inserirdesenvolvedorpelorelacionamentodeprojeto', function() {
    $projeto = Projeto::with('desenvolvedores')->find(4);

    //$projeto->desenvolvedores()->attach(1, ['horas_semanais' => 20]); //Inclusão do desenvolvedor 1 e projeto 4 na tabela alocacoes com o valor de horas semanais de 20
    $projeto->desenvolvedores()->attach([
        1 => ['horas_semanais' => 20],
        2 => ['horas_semanais' => 60],
        3 => ['horas_semanais' => 40],
    ]);
});

Route::get('/removerdesenvolvedorpelorelacionamentodeprojeto', function() {
    $projeto = Projeto::with('desenvolvedores')->find(4);

    $projeto->desenvolvedores()->detach([1, 3]);
});