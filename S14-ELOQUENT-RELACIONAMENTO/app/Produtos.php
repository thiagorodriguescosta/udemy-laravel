<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produtos extends Model
{
    public function categorias()
    {
        return $this->belongsTo('App\Categorias', 'categoria_id', 'id');
    }
}
