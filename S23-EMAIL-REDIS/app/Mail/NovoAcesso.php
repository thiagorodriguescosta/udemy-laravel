<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Auth\User;

class NovoAcesso extends Mailable
{
    use Queueable, SerializesModels;

    protected $user;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.novoacesso')
            ->with([
                'name' => $this->user->name,
                'email' => $this->user->email,
                'datahora' => now()
                    ->setTimezone('America/Sao_Paulo')
                    ->format('d-m-Y H:i:s')
            ])
            ->attach(base_path() . '/arquivo/aquivo_qualquer.pdf');
    }
}
