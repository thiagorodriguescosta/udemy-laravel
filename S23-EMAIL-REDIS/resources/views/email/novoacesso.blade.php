<!DOCTYPE html>
<body>
    <h4>Seja bem vindo {{ $name }}</h4>
    <p>Obrigado pela preferência</p>
    <p>Email: {{ $email }}</p>
    <p>Data Hora: {{ $datahora }}</p>
    <p>Imagem 
        <img width="10%" height="10%" src="{{ $mensagem->embed( public() ) . '/img/laravel.png' }}" alt="Imagem">
    </p>
</body>
</html>