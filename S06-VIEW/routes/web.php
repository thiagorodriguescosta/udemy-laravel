<?php

Route::get('/', function () {
    return view('filho');
});

Route::get('/meucomponente', function () {
    return view('meucomponente');
});

Route::get('/novoalerta', function () {
    return view('testar_novoalerta');
});

Route::get('/primeiraview', function() {
    return view('primeiraview');
});

Route::get('/ola', function() {
    // return view('minhaview', ['nome'=>'Joao Paulo']);
    // ou
    return view('minhaview')->with('nome','Joao')->with('sobrenome', 'Paulo');

});

Route::get('/ola/{nome}/{sobrenome}', function($nome, $sobrenome) {
    return view('minhaview', ['nome'=> $nome, 'sobrenome' => $sobrenome]);
    // ou
    // return view('minhaview', compact('nome', 'sobrenome'));
});

Route::get('/email/{email}', function($email) {
    if (View::exists('email')) {
        return view('email', ['email'=> $email]);
    }
    else
        return view('erro');
});

Route::get('/produtos', 'ProdutoControlador@listar');

Route::get('/secaoprodutos/{palavra}', 'ProdutoControlador@secao_produtos');

Route::get('/mostraropcoes', 'ProdutoControlador@mostrar_opcoes');

Route::get('/opcoes/{opcao}', 'ProdutoControlador@opcoes');

Route::get('/loop/for/{n}', function($n) {
    return view('loop_for', compact('n'));
});

Route::get('/loop/foreach', function() {
    $produtos = [ 
        [ "id" => 1, "nome" => "computador" ],
        [ "id" => 2, "nome" => "mouse" ],
        [ "id" => 3, "nome" => "impressora" ],
        [ "id" => 4, "nome" => "monitor" ],
        [ "id" => 5, "nome" => "teclado" ],
    ];
    return view('loop_foreach', compact('produtos'));
});

