@extends('layout.app', ["current" => "produtos" ])

@section('body')
<div class="card border">
    <div class="card-body">
        <h5 class="card-title">Listagem de Produtos</h5>
        <table id="tabelaProdutos" class="table table-ordered table-hover">
            <thead>
                <tr>
                    <th>Código</th>
                    <th>Nome</th>
                    <th>Quantidade</th>
                    <th>Preço</th>
                    <th>Categoria</th>
                    <th>Ações</th>
                </tr>
            </thead>
            <tbody>    
            </tbody>
        </table>
    </div>
    <div class="card-footer">
        <button class="btn btn-sm btn-primary" onClick="novoProduto()" role="button">Nova produto</a>
    </div>
</div>

<div class="modal fade" id="modelId" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form id="formCadastrarProduto" action="" method="post">
                <div class="modal-header">
                    <h5 class="modal-title">Novo Produto</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="id" id="id">
                    <div class="form-group">
                        <label for="nome">Nome do Produto</label>
                        <input type="text" name="nome" id="nome" class="form-control" placeholder="Nome do Produto" aria-describedby="helpId">
                    </div>
                    <div class="form-group">
                        <label for="quantidade">Quantidade</label>
                        <input type="number" name="quantidade" id="quantidade" class="form-control" placeholder="Quantidade do Produto" aria-describedby="helpId">
                    </div>
                    <div class="form-group">
                        <label for="preco">Preço</label>
                        <input type="number" name="preco" id="preco" class="form-control" placeholder="Preço do Produto" aria-describedby="helpId">
                    </div>
                    <div class="form-group">
                        <label for="categoria">Categoria</label>
                        <select class="form-control" name="categoria_id" id="categoria">
                            <option value="">Selecione...</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Salvar</button>
                    <button type="cancel" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('javascript')
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': "{{ csrf_token() }}"
        }
    })

    function novoProduto() {
        $('#nome').val('')
        $('#quantidade').val('')
        $('#preco').val('')
        $('#modelId').modal('show')
    }

    $(function() {
        createOptionsCategoriesSelect()
        createTableProducts()
    })

    function createOptionsCategoriesSelect() {
        $.getJSON('/api/categorias', function(categories) {
            for(i = 0; i < categories.length; i++) {
                $("#categoria").append('<option value="' + categories[i].id + '">' + categories[i].nome + '</option>')
            }
        })
    }

    function createTableProducts() {
        $.getJSON('/api/produtos', function(products) {
            for(i = 0; i < products.length; i++) {
                row = createRow(products[i])
                $("#tabelaProdutos > tbody").append(row)
            }
        });
    }

    function createRow(product) {
        const row = 
        '<tr>' +
            '<td>' + product.id + '</td>' +
            '<td>' + product.nome + '</td>' +
            '<td>' + product.estoque + '</td>' +
            '<td>' + product.preco + '</td>' +
            '<td>' + product.categoria_id + '</td>' +
            '<td>' + 
                '<button class="btn btn-primary" onclick="getProduto(' + product.id + ')">Editar</button> ' +
                '<button class="btn btn-danger" onclick="removerProduto(' + product.id + ')">Excluir</button>' +
            '</td>' +
        '</tr>';

        return row
    }

    function getProduto(id) {
        $.getJSON('/api/produtos/' + id, function(product) {
            $('#id').val(product.id)
            $('#nome').val(product.nome)
            $('#quantidade').val(product.estoque)
            $('#preco').val(product.preco)
            $('#categoria').val(product.categoria_id)
            $('#modelId').modal('show')
        });
    }

    $('#formCadastrarProduto').submit(function(event) {
        event.preventDefault();
        const idProduct = $('#id').val();
        if (idProduct != '') {
            alterarProduto(idProduct)
        } else {
            cadastrarProduto()
        }
        $('#modelId').modal('hide')
    })
    
    function cadastrarProduto() {
        const produto = {
            nome: $('#nome').val(),
            preco: $('#preco').val(),
            estoque: $('#quantidade').val(),
            categoria_id: $('#categoria').val()
        }
        $.post('/api/produtos', produto, function(data){
            const row = createRow(data)
            $("#tabelaProdutos > tbody").append(row)
        })
    }

    function alterarProduto(id) {
        produto = {
            nome: $('#nome').val(),
            estoque: $('#quantidade').val(),
            preco: $('#preco').val(),
            categoria_id: $('#categoria').val()
        }

        $.ajax({
            type: "PUT",
            url: "/api/produtos/" + id,
            data: produto,
            context: this,
            success: function(result) {
                const rows = $('#tabelaProdutos > tbody > tr');
                e = rows.filter(function(i, element) {
                    return element.cells[0].textContent == id
                })
                if (e) {
                    e[0].cells[0].textContent = result.id;
                    e[0].cells[1].textContent = result.nome;
                    e[0].cells[2].textContent = result.estoque;
                    e[0].cells[3].textContent = result.preco;
                    e[0].cells[4].textContent = result.categoria_id;
                }
            },
            error: function(error) {
                console.log(error)
            }
        })
    }

    function removerProduto(id) {
        $.ajax({
            type: "DELETE",
            url: "/api/produtos/" + id,
            context: this,
            success: function(result) {
                const rows = $('#tabelaProdutos > tbody > tr');
                e = rows.filter(function(i, element) {
                    return element.cells[0].textContent == id
                })
                if (e) {
                    e.remove()
                }
            },
            error: function(error) {
                console.log(error)
            }
        })
    }
    
</script>    
@endsection