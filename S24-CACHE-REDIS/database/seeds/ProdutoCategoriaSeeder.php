<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\ProdutoCategoria;
use App\Produto;
use App\Categoria;

class ProdutoCategoriaSeeder extends Seeder
{
    public function run()
    {
        $faker = Faker::create();

        $produtos = Produto::all();
        $categorias = Categoria::all()->pluck('id')->toArray();

        foreach($produtos as $produto) {
            $elementos = rand(2, 6);
            for($i = 0; $i < $elementos; $i++) {
                do {
                    $cat_id = $faker->randomElement($categorias);
                } while (ProdutoCategoria::where('produto_id', $produto->id)
                    ->where('categoria_id', $cat_id->exist()));
                
                ProdutoCategoria::created([
                    'categoria_id' => $cat_id,
                    'produto_id' => $produto->id
                ]);
            }
        }

    }
}
