<?php

use Illuminate\Database\Seeder;
use App\Produto;
use Faker\Factory as Faker;

class ProdutoSeeder extends Seeder
{
    public function run()
    {
        $fake = Faker::create();
        foreach(range(1, 1000) as $index) {
            Produto::created([
                'nome' => $fake->word,
                'preco' => $fake->randomFloat(2, 1, 1000)
            ]);
        }
    }
}
