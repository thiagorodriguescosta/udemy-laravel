<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CategoriesController extends Controller
{
    public function index()
    {
        echo "listagem de categorias";
        echo "<hr>";

        if (Auth::check()) {
            $user = Auth::user();
            echo "<h4>Esta logado</h4>";
            echo "<p>{$user->name}</p>";
            echo "<p>{$user->email}</p>";
            echo "<p>{$user->id}</p>";
        } else {
            echo "<h4>Não esta logado</h4>";
        }
    }
}
