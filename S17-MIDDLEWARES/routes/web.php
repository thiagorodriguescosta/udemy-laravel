<?php

/**
 *  Utilizando direto da classe
 */
use App\Http\Middleware\PrimeiroMiddleware;

Route::get('/chamandoMiddlewareDireto', 'UsuarioController@index')->middleware(PrimeiroMiddleware::class);

/**
 * Utilizando direto arquivo Kernel.php
 */
Route::get('/chamandoMiddlewarePelaClassKernel', 'UsuarioController@index')->middleware('primeiro');


Route::get('/chamandoMiddlewarePeloContostrutor', 'UsuarioController@index');

/**
 * Chamando Middleware em todas as rotas
 */
Route::get('/rotaqualquer', function(){
    return 'debug';
});


/**
 * Chamando o 'primeiro' e 'segundo' Middleware
 */
Route::get('/chamandodoismiddlewares', function() {
    return 'Chamando 2 middlewares';
})->middleware(['primeiro', 'segundo']);

