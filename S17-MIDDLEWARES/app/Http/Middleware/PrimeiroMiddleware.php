<?php

namespace App\Http\Middleware;

use Closure;
use function Psy\debug;
use Illuminate\Support\Facades\Log;

class PrimeiroMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        Log::debug('Passei aqui ein');
        return $next($request);
    }
}
