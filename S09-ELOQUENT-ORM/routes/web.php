<?php
use App\Categoria;

Route::get('/listarcategorias', function () {
    $categorias = Categoria::all();
    foreach ($categorias as $categoria) {
        echo "id: {$categoria->id}, nome: {$categoria->nome} <br>";
    }

});

Route::get('/inserircategorias/{nome}', function($nome) {
    $categoria = new Categoria();

    $categoria->nome = $nome;
    $categoria->save();

    return redirect('/listarcategorias');
});

Route::get('/visualizarcategoria/{id}', function($id) {
    // $categoria = Categoria::find($id);
    $categoria = Categoria::findOrFail($id);

    echo "id: {$categoria->id}, nome: {$categoria->nome} <br>";
});

Route::get('/atualizarcategoria/{id}/{nome}', function($id, $nome) {
    // $categoria = Categoria::find($id);
    $categoria = Categoria::findOrFail($id);
    
    $categoria->nome = $nome;
    $categoria->save();

    return redirect('/listarcategorias');
});

Route::get('/removercategoria/{id}', function($id) {
    $categoria = Categoria::find($id);

    if (isset($categoria)) {
        $categoria->delete();
        return redirect('/listarcategorias');
    } else {
        echo "Categoria não encontrada";
    }
});

Route::get('/listartodascategorias', function() {
    $categorias = Categoria::withTrashed()->get();

    foreach ($categorias as $categoria) {
        echo "id: {$categoria->id}, nome: {$categoria->nome}";
        if ($categoria->trashed()) {
            echo " (deletado) <br>";
        } else {
            echo "<br>";
        }
    }
});

Route::get('/visualizarcategoriaporidinclusivepagadas/{id}', function($id) {
    // $categoria = Categoria::withTrashed()->find($id);
    $categoria = Categoria::withTrashed()->where('id', $id)->get()->first();

    if (isset($categoria)) {
        echo "id: {$categoria->id}, nome: {$categoria->nome} <br>";
    } else {
        echo "Categoria não encontrada";
    }
});

Route::get('/listarcategoriasapagadas', function() {
    $categoria = Categoria::onlyTrashed()->get();

    foreach ($categoria as $categoria) {
        echo "id: {$categoria->id}, nome: {$categoria->nome}";
        if ($categoria->trashed()) {
            echo " (deletado) <br>";
        } else {
            echo "<br>";
        }
    }
});

Route::get('/restaurarcategoria/{id}', function($id) {
    $categoria = Categoria::withTrashed()->find($id);

    if (isset($categoria)) {
        $categoria->restore();
        echo "<a href='/listarcategorias'>Listar Categorias</a>";
    } else {
        echo "Categoria não encontrada";
    }
});

Route::get('/apagarcategoriapermatentemente/{id}', function($id) {
    $categoria = Categoria::find($id);

    if (isset($categoria)) {
        $categoria->forceDelete();
        echo "<a href='/listarcategorias'>Listar Categorias</a>";
    } else {
        echo "Categoria não encontrada";
    }
});